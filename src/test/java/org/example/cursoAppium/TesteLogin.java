package org.example.cursoAppium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;

public class TesteLogin {

    private static ScreenLogin screenLogin;
    private static AppiumDriver<RemoteWebElement> driver;

    @BeforeClass //executado antes do teste
    public static void caps() throws MalformedURLException{
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("app", new File("apps/app-debug.apk"));
        capabilities.setCapability("deviceName", "emulator-5554"); //para pegar o nome: adb devices no terminal
        capabilities.setCapability("platformName", "Android");
//        capabilities.setCapability("platformVersion", "11.3");//para iOS precisa desse parametro
//        capabilities.setCapability("automationName", "XCUITest");//para iOS precisa desse parametro - framework de teste nativo do iOS
        capabilities.setCapability("unicodeKeyboard", true); //para o iOS n�o usa esse par�metro

        driver = new AndroidDriver<RemoteWebElement>(new URL("http://localhost:4723/wd/hub"), capabilities);
//        driver = new IOSDriver<RemoteWebElement>(new URL("http://localhost:4723/wd/hub"), capabilities); //para iOS
        screenLogin = new ScreenLogin(driver);
    }

    @Test
    public void testeLogin(){
        screenLogin.logar();
    }
}
