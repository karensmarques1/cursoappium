package org.example.cursoAppium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import java.net.MalformedURLException;
import java.net.URL;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;

public class DeviceFarmLogin {

    private static ScreenLogin screenLogin;
    private static AppiumDriver<RemoteWebElement> driver;

    @BeforeClass //executado antes do teste
    public static void caps() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        // Set your access credentials
        capabilities.setCapability("browserstack.user", "karendesouzamarq_f4FsgO");
        capabilities.setCapability("browserstack.key", "aVYDVEzXq1ruEGHMjw1c");

        // Set URL of the application under test
        capabilities.setCapability("app", "bs://61a31c9c65aa61604a037421c98a0c449aa6f460");

        // Specify device and os_version for testing
        capabilities.setCapability("device", "Google Pixel 3");
        capabilities.setCapability("os_version", "9.0");

        driver = new AndroidDriver<RemoteWebElement>(new URL("http://hub.browserstack.com/wd/hub"), capabilities);
        screenLogin = new ScreenLogin(driver);
    }

    @Test
    public void testeLogin(){
        screenLogin.logar();
    }
}
