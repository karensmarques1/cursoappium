package org.example.cursoAppium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;

public class ScreenLogin {

    //identifica os elementos e a plataforma
    public ScreenLogin(AppiumDriver<RemoteWebElement> driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    //vai usar android ou ios conforme driver configurado
    @AndroidFindBy(id = "login_username")
    @iOSFindBy(accessibility = "")
    private RemoteWebElement campoEmail;

    @AndroidFindBy(id = "login_password")
    @iOSFindBy(accessibility = "")
    private RemoteWebElement campoSenha;

    @AndroidFindBy(id = "login_button")
    @iOSFindBy(accessibility = "")
    private RemoteWebElement botaoContinuar;

    public void logar() {
        campoEmail.sendKeys("teste@email.com");
        campoSenha.sendKeys("123456");
        botaoContinuar.click();
    }
}
